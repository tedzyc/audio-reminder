package com.zyc.ui;

import com.zyc.R;
import com.zyc.misc.Constant;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.Calendar;

/**
 * @author ZengYongchang@gmail.com
 */
public class RemindItemCursorAdapter extends CursorAdapter {
    private Calendar mCalendar;


    public RemindItemCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        mCalendar = Calendar.getInstance();
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_view_item_layout, null);
        ViewHolder holder = new ViewHolder();
        holder.tvStartRecordingTime = (TextView) view.findViewById(R.id.startRecordingTime);
        holder.tvRecordLength = (TextView) view.findViewById(R.id.recordLength);
        holder.tvExpireTime = (TextView) view.findViewById(R.id.expireTime);
        holder.tvPriority = (TextView) view.findViewById(R.id.priority);
        view.setTag(holder);
        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long startTime = cursor.getLong(cursor.getColumnIndex(Constant.Db.START_RECORDING_TIME_COLUMN));
        long stopTime = cursor.getLong(cursor.getColumnIndex(Constant.Db.STOP_RECORDING_TIME_COLUMN));
        long expireTime = cursor.getLong(cursor.getColumnIndex(Constant.Db.EXPIRE_TIME_COLUMN));
        holder.tvStartRecordingTime.setText(makeStartRecordingTimeShowingText(startTime));
        holder.tvRecordLength.setText(makeRecordLengthShowingText(startTime, stopTime));
        holder.tvExpireTime.setText(makeExpireTimeShowingText(expireTime));
        holder.tvPriority.setText(Integer.toString(cursor.getInt(cursor.getColumnIndex(Constant.Db.PRIORITY_COLUMN))));
    }


    private String makeStartRecordingTimeShowingText(long millisecond) {
        mCalendar.setTimeInMillis(millisecond);
        return TimeStrConverter.prefixStr(mCalendar.get(Calendar.HOUR_OF_DAY)) + ":"
               + TimeStrConverter.prefixStr(mCalendar.get(Calendar.MINUTE)) + "_"
               + TimeStrConverter.toEnglishMonthStr(mCalendar.get(Calendar.MONTH))
               + TimeStrConverter.prefixStr(mCalendar.get(Calendar.DAY_OF_MONTH));
    }


    private String makeRecordLengthShowingText(long startMillisecond, long stopMillisecond) {
        int lengthSecond = (int)(stopMillisecond - startMillisecond)/1000;
        int minute = (int)(lengthSecond/60);
        int restSecond = lengthSecond - minute*60;
        return minute + ":" + TimeStrConverter.prefixStr(restSecond); 
    }


    private String makeExpireTimeShowingText(long millisecond) {
        mCalendar.setTimeInMillis(millisecond);
        return TimeStrConverter.prefixStr(mCalendar.get(Calendar.HOUR_OF_DAY)) + ":"
               + TimeStrConverter.prefixStr(mCalendar.get(Calendar.MINUTE)) + "_"
               + TimeStrConverter.prefixStr(mCalendar.get(Calendar.DAY_OF_MONTH)) + "#";
    }



    private static final class ViewHolder {
        private TextView tvStartRecordingTime;
        private TextView tvRecordLength;
        private TextView tvExpireTime;
        private TextView tvPriority;
    }



    private static class TimeStrConverter {
        public static String prefixStr(int param) {
            return (param < 10) ? ("0" + Integer.toString(param)) : Integer.toString(param);
        }

        public static String toEnglishMonthStr(int month) {
            switch(month) {
                case 0:
                    return "Jan";
                case 1:
                    return "Feb";
                case 2:
                    return "March";
                case 3:
                    return "April";
                case 4:
                    return "May";
                case 5:
                    return "June";
                case 6:
                    return "July";
                case 7:
                    return "Aug";
                case 8:
                    return "Sep";
                case 9:
                    return "Oct";
                case 10:
                    return "Nov";
                case 11:
                    return "Dec";
                default:
                    return "invalid_month";
            }
        }
    }
}
