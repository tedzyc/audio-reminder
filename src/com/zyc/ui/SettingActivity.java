package com.zyc.ui;

import com.zyc.R;

import android.app.Activity;
import android.os.Bundle;

/**
 * @author ZengYongchang@gmail.com
 */
public class SettingActivity extends Activity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
    }
}
