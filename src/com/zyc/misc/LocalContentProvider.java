package com.zyc.misc;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * @author ZengYongchang@gmail.com
 */
public class LocalContentProvider extends ContentProvider {
    private DatabaseHelper mOpenHelper;
    private static final UriMatcher sUriMatcher;
    
    
    
    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(Constant.Db.AUTHORITY, "audio_reminder_table", Constant.Db.AUDIO_REMINDER_OF_MATCH_CODE_1);
        sUriMatcher.addURI(Constant.Db.AUTHORITY, "audio_reminder_table/#", Constant.Db.AUDIO_REMINDER_ID_OF_MATCH_CODE_2);
    }

    
    
    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (sUriMatcher.match(uri)) {
            case Constant.Db.AUDIO_REMINDER_OF_MATCH_CODE_1:
                qb.setTables(Constant.Db.TABLE_NAME);
                break;
            case Constant.Db.AUDIO_REMINDER_ID_OF_MATCH_CODE_2:
                qb.setTables(Constant.Db.TABLE_NAME);
                qb.appendWhere(Constant.Db._ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        Cursor cursor = qb.query(mOpenHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    
    @Override
    public String getType(Uri uri) {
        return null;
    }
    

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        if (sUriMatcher.match(uri) != Constant.Db.AUDIO_REMINDER_OF_MATCH_CODE_1)
            throw new IllegalArgumentException("Unknown URI " + uri);
        ContentValues values = (initialValues != null) ? (new ContentValues(initialValues)) : (new ContentValues()); 
        long rowId = mOpenHelper.getWritableDatabase().insert(Constant.Db.TABLE_NAME, Constant.Db.START_RECORDING_TIME_COLUMN, values);
        return ContentUris.withAppendedId(Constant.Db.CONTENT_URI, rowId);
    }

    
    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        return mOpenHelper.getWritableDatabase().delete(Constant.Db.TABLE_NAME, Constant.Db._ID + "=" + uri.getPathSegments().get(1), null);
    }

    
    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int updatedRowId = mOpenHelper.getWritableDatabase().update(Constant.Db.TABLE_NAME, values, Constant.Db._ID + "=" + uri.getPathSegments().get(1), null);
        getContext().getContentResolver().notifyChange(uri, null);
        return updatedRowId;
    }

    
    
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, Constant.Db.DATA_BASE_NAME, null, Constant.Db.DATA_BASE_VERSION);
        }
    
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Constant.Db.CREATE_TABLE_STATEMENT);
        }
    
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Constant.Db.TABLE_NAME);
            onCreate(db);
        }
    }
}
