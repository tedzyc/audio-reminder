package com.zyc.misc;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

/**
 * @author ZengYongchang@gmail.com
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Uri uri = intent.getData();
        Cursor cursor = context.getContentResolver().query(uri, new String[]{ Constant.Db.AUDIO_FILE_PATH_COLUMN }, null, null, null);
        cursor.moveToFirst();
        (new AudioUtil()).playAudio(cursor.getString(cursor.getColumnIndex(Constant.Db.AUDIO_FILE_PATH_COLUMN)));
        cursor.close();

        ContentValues value = new ContentValues();
        value.put(Constant.Db.IS_EXECUTED_COLUMN, true);
        context.getContentResolver().update(uri, value, null, null);
    }
}
