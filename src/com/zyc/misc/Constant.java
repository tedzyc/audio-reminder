package com.zyc.misc;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * @author ZengYongchang@gmail.com
 */
public class Constant {
    // AudioRecordActivity.java
    public static final int RECORD_LENGTH_LIMIT_IN_MILLISECOND = 8000;
    public static final int MENU_ITEM_SETTING = 1;
    public static final String AUDIO_FILE_POSTFIX = ".amr";

    // ItemSetAndLaunchActivity.java
    // TODO: Further change below value to 2 or something like that.
    public static final int DEFAULT_HOUR_INCREMENT = 0;

    // TimerUtil.java
    public static final int MESSAGE_TIMER_COMPLETE = 1;



    public static class Db implements BaseColumns {
        public static final String IS_EXECUTED_COLUMN = "is_executed";
        public static final String START_RECORDING_TIME_COLUMN = "start_recording_time";
        public static final String STOP_RECORDING_TIME_COLUMN = "stop_recording_time";
        public static final String AUDIO_FILE_PATH_COLUMN = "audio_file_path";
        public static final String EXPIRE_TIME_COLUMN = "expire_time";
        public static final String PRIORITY_COLUMN = "priority";

        public static final int DATA_BASE_VERSION = 1;
        public static final String DATA_BASE_NAME = "audio_reminder_data_base";
        public static final String TABLE_NAME = "audio_reminder_table";
        
        public static final String AUTHORITY = "com.zyc.misc.LocalContentProvider";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        
        public static final int AUDIO_REMINDER_OF_MATCH_CODE_1 = 1;
        public static final int AUDIO_REMINDER_ID_OF_MATCH_CODE_2 = 2;
        
        // TODO change table name
        public static final String CREATE_TABLE_STATEMENT = "CREATE TABLE " + TABLE_NAME
        + " (_id INTEGER PRIMARY KEY AUTOINCREMENT,"
        + " is_executed BIT,"
        + " start_recording_time INTEGER,"
        + " stop_recording_time INTEGER,"
        + " audio_file_path TEXT,"
        + " expire_time INTEGER,"
        + " priority INTEGER);";
    }
}
